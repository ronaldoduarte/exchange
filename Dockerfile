FROM python:3.7.2-stretch

ENV APP /app

COPY requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt

RUN mkdir $APP
WORKDIR $APP

COPY . .

CMD ["uwsgi", "--ini", "uwsgi.ini"]
