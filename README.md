# Exchange app

## Getting started
- Clone this repository
- Copy local.env to .env. You may use the default values, or choose suitable username and password. The only missing variable is the Open Exchange Rate API key, it must be placed after the `OXR_API_KEY` variable, e.g., `OXR_API_KEY=77489237787acd7878` (invalid key, used just for demonstration purposes).
- From the root repository call *docker-compose up*
- After the services are up, migrations must be applied to the database: *docker-compose exec app flask db upgrade*
- API is reachable at port 80 on the localhost.

## Using the API

### /last/ endpoint ###
It accepts to parameters in the querystring:
- `limit=n`: brings the last n exchange queries made, in descending chronological order
- `currency=<3 letter ISO code>`: apply currency filter

Example: */last/currency=BRL&limit=3* brings only the last three queries made with Brazilian Reais.

Example request with cURL:

`curl http://localhost/last/?limit=3&currency=BRL`

Example response payload:
```
{
    "currency": "BRL",
    "data": [
        {
            "amount": "700.00000000",
            "amount_in_usd": "187.98501763",
            "created_at": "2019-02-20T04:16:56+00:00",
            "currency": "BRL",
            "rate": "3.72370101"
        }
    ],
    "limit": 3,
    "source": "database"
}
```
`source` indicates the data source, `database` (MySQL) or `cache` (redis).

### /grab_and_save/ endpoint ###
It accepts two parameters in the JSON payload:

- `amount`: the amount to be converted to USD
- `currency`: the amount currency code

Example request payload:
```
{
    "amount": 700.00,
    "currency": "BRL"
}
```

Example request with cURL:

`curl --header "Content-Type: application/json" --request POST --data '{"amount": 543, "currency": "EUR"}' http://localhost/grab_and_save/`

Example response payload:
```
{
    "amount": "700.00000000",
    "amount_in_usd": "187.98501763",
    "created_at": "2019-02-20T04:16:56+00:00",
    "currency": "BRL",
    "rate": "3.72370101"
}
```
