from prettyconf import config

db_uri_template = 'mysql+pymysql://{user}:{password}@{host}/{database}'


class Config:
    OXR_API_KEY = config('OXR_API_KEY')
    REDIS_HOST = config('REDIS_HOST')

    SQLALCHEMY_DATABASE_URI = db_uri_template.format(
        user=config('DATABASE_USER'),
        password=config('DATABASE_PASSWORD'),
        host=config('DATABASE_HOST'),
        database=config('DATABASE_NAME'),
    )

    SQLALCHEMY_TRACK_MODIFICATIONS = False
