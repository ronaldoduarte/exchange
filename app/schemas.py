import decimal

from marshmallow import fields, Schema, validates, ValidationError


class GrabAndSaveSchema(Schema):
    currency = fields.Str(required=True)
    amount = fields.Decimal(places=8, rounding=decimal.ROUND_UP, required=True)

    @validates('currency')
    def validate_currency(self, value):
        if len(value) != 3:
            raise ValidationError('Currency must be a ISO3 currency code.')


class GetLastSchema(Schema):
    created_at = fields.DateTime(required=True)
    amount = fields.Decimal(places=8, rounding=decimal.ROUND_UP, as_string=True, required=True)
    currency = fields.Str(required=True)
    rate = fields.Decimal(places=8, rounding=decimal.ROUND_UP, as_string=True, required=True)
    amount_in_usd = fields.Decimal(places=8, rounding=decimal.ROUND_UP, as_string=True, required=True)
