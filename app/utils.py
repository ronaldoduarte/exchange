import json

import redis
import requests

from app import app

redis_client = redis.Redis(host=app.config['REDIS_HOST'])

def get_exchange_rates():
    url = 'https://openexchangerates.org/api/latest.json'
    params = {'app_id': app.config['OXR_API_KEY']}

    try:
        response = requests.get(url, params=params, timeout=5)
    except requests.exceptions.Timeout:
        return {}

    if response.status_code != 200:
        return {}

    return response.json()['rates']


def add_to_cache(data):
    queries = json.loads(redis_client.get('queries') or '[]')
    queries.append(data)
    redis_client.set('queries', json.dumps(queries))


def get_from_cache(limit=1, currency=None):
    queries = redis_client.get('queries')
    if queries:
        queries = json.loads(queries)
        if currency:
            queries = [q for q in queries if q['currency'] == currency]

        if len(queries) < limit:
            return []

        return queries[-1:-limit-1:-1]

    return []
