from datetime import datetime

from app import db


class ExchangeQuery(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    created_at = db.Column(db.DateTime, nullable=False, index=True, default=datetime.utcnow)
    amount = db.Column(db.Numeric(precision=32, scale=8), nullable=False)
    currency = db.Column(db.String(3), nullable=False, index=True)
    rate = db.Column(db.Numeric(precision=32, scale=8), nullable=False)
    amount_in_usd = db.Column(db.Numeric(precision=32, scale=8), nullable=False)

    def save(self):
        db.session.add(self)
        db.session.commit()
        return self
