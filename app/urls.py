from app import app
from app.views import GetLastView, GrabAndSaveView


app.add_url_rule('/grab_and_save/', view_func=GrabAndSaveView.as_view('grab_and_save'))
app.add_url_rule('/last/', view_func=GetLastView.as_view('last'))
