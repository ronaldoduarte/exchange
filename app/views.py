from decimal import Decimal, ROUND_UP

from flask import request, jsonify
from flask.views import MethodView

from app import app
from app.orm.models import ExchangeQuery
from app.schemas import GetLastSchema, GrabAndSaveSchema
from app.utils import add_to_cache, get_from_cache, get_exchange_rates

def create_error_message(message):
    return jsonify({'error': message})


def quantize(number):
    return number.quantize(Decimal('1.00000000'), rounding=ROUND_UP)


class GrabAndSaveView(MethodView):
    def post(self):
        post_data = request.get_json() or {}
        data, errors = GrabAndSaveSchema().load(post_data)

        if errors:
            return jsonify(errors), 400

        amount = data['amount']
        currency = data['currency']

        rates = get_exchange_rates()
        if not rates:
            return create_error_message('Service unavailable'), 503

        rate = rates.get(currency)
        if not rate:
            return create_error_message(f'currency {currency} not available')

        rate = quantize(Decimal(rate))
        amount_in_usd = quantize(amount / rate)

        query_data = {
            'amount': amount,
            'currency': currency,
            'rate': rate,
            'amount_in_usd': amount_in_usd,
        }

        query = ExchangeQuery(**query_data).save()
        query_data['created_at'] = query.created_at
        query_data = GetLastSchema().dump(query_data).data
        add_to_cache(query_data)

        return jsonify(query_data), 200


class GetLastView(MethodView):
    def get(self):
        limit = int(request.args.get('limit', 1))
        limit = limit if limit > 0 else 1
        currency = request.args.get('currency')

        response = {
            'currency': currency,
            'limit': limit,
        }

        cached_data = get_from_cache(limit, currency)
        if cached_data:
            response['source'] = 'cache'
            response['data'] = cached_data
        else:
            qs = ExchangeQuery.query.order_by(ExchangeQuery.created_at.desc())

            if currency:
                qs = qs.filter_by(currency=currency)
            qs = qs.limit(limit).all()

            response['source'] = 'database'
            response['data'] = GetLastSchema().dump(qs, many=True).data

        return jsonify(response), 200
